---
layout: markdown_page
title: "Hiring"
---
* [Hiring Process](#hiring-process)

## Hiring Process<a name="hiring-process"></a>

1. Create job description.
    * The CEO needs to authorize any new job positions/searches, and agree on the proposed hiring team.
      * A rough estimate of preferred range of compensation, or some other way of dealing with that inevitable interview question, needs to be determined.
    * The description will go on https://about.gitlab.com/jobs site, and will consist of:
      * Job title
      * Preferred geographic region where candidate should reside
      * Pre-amble about GitLab as a company (alternative: the pre-amble may be placed at the top of the jobs page and omitted from the individual descriptions).
      * Description of role
      * Requirements for the role (can be split into must-have’s and nice-to-have’s)
      * Post-amble stating how to apply, and who the hiring manager is.
1. Define hiring team.
    * Roles to be assigned are: (one person can of course handle multiple roles)
      * Person(s) to do first vetting of candidates, selecting applicants  for interview.
      * Person(s) to have (first round) interviews.
      * Optional: Person(s) to have second round interviews.
      * Person(s) to make final decision to make offer.
      * Person(s) actually making the offer, including terms of offer.
      * Person(s) to handle communications with applicants along the way.
1. Define a hiring timeline.
    * Choose from specific deadlines (e.g. all applicants will be reviewed on date X, hear back by date Y), or
    * Choose rolling application process, wherein the review and interview process  happen as applications come in, or
    * Some combination of the above. So for example, wait for X amount of time to gather enough applications, then review in bulk, set up first interviews, and repeat this process until a suitable applicant is found.
1. Publish the job description.
    * Confirm that the CEO (or person authorized by CEO) has signed off on the description, hiring team, and timeline.
    * Create the job posting within the [Workable](https://gitlab.workable.com/backend) tool. The job will then automatically appear on the [GitLab Jobs](https://about.gitlab.com/jobs) site.
1. Optional: advertise the job description.
    * This can be through “soft” referral, e.g. all GitLab staff post link to jobs site on their LinkedIn profiles.
    * And/Or it can be through job boards.
    * Use the [Workable Clipper](http://resources.workable.com/the-workable-clipper) to help source candidates directly from LinkedIn, and  familiarize yourself with the Workable environment, work flow, features, and support desk.
1. Interview Questions.
    * Hiring team to determine which questions need to be asked, and by whom in the team.
    * Homework assignments may be required for some positions.
1. Communication with Applicants
    * Upon receiving the application and reviewing it for the first time:
      * Applicants should receive confirmation of their application, thanking them for submitting their information. This may be an automated message.
      * If information is missing and the applicant seems sufficiently promising (or not enough information to be able to make that determination), the appropriate person from the hiring team should follow up requesting additional information.
    * Timing
      * Interviews should be set up in accordance with the hiring timeline that was defined previously; and applicants should be notified of this process as much as possible.
      * At time of interview, applicant should be told what the timeline is for a decision, and what the next steps are (if any). An example message would be "We are reviewing applications through the end of next week, and will let you know by the end of two weeks from today whether you've been selected for the next round or not. Please feel free to ping us if you haven't heard anything from us by then."
    * Feedback
      * Currently, the CEO needs to review feedback to candidates who have not been selected.
1. Make a decision, make an offer.
    * The CEO needs to authorize offers.
    * Sign up successful applicant and move to onboarding.
    * Inform other applicants that we selected someone else this time. Applicants remain in the database and may be contacted in the future for other roles.

