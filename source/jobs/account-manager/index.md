---
layout: markdown_page
title: "Account Manager Responsibilities and Tasks"
---

## Responsibilities

* Manage relationships with prospective and current customers
* Manage new and existing deals with customers
* Grow existing deals with our customers

* Senior Account Managers: Take the lead in large deals
