---
layout: markdown_page
title: "Developer Evangelist Responsibilities and Tasks"
---

## Responsibilities

* Create developer tutorials and content tools to help our community go from idea to launch. 
* Work closely with the marketing team to identify and create events that are relevant for the GitLab community.
* You don’t mind hanging out at a university hackathon for the weekend.
* Speak to developers all around the world (in person or virtual) who are interested in using GitLab.
* Recognize trends in feedback from the community and relay that information back to our engineering and PM team.
* Put together technical presentations that keep attendees engaged and interested.
* Get on calls with our sales team and our customers to help solve a complex problem.