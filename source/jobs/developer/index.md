---
layout: markdown_page
title: "Developer Responsibilities and Tasks"
---

## Responsibilities

* Bring a new feature from vague request to polished end result
* Support our service engineers in getting to the bottom of user-reported issues and come up with robust solutions
* Engage with the rest of the core team and the open source community and collaborate on improving GitLab
* Review code contributed by the community and work with them to get it ready for production
* Write documentation around features and configuration to save our users time
* Take initiative in improving the software in small or large ways to address pain points in your own experience as a developer
* Keep code easy to maintain and understand to keep the barrier for contribution low